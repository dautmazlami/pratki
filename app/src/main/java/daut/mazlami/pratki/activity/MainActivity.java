package daut.mazlami.pratki.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import daut.mazlami.pratki.R;
import daut.mazlami.pratki.fragment.LocationFragment;
import daut.mazlami.pratki.fragment.MyDeliveriesFragment;
import daut.mazlami.pratki.fragment.MyPostFragment;
import daut.mazlami.pratki.fragment.TrackingFragment;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.setSelectedItemId(R.id.menu_item_search);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        switch (menuItem.getItemId()){
            case R.id.menu_item_search:
                ft.replace(R.id.frame_layout, TrackingFragment.newInstance(),TrackingFragment.TAG);
                ft.commit();
                return true;
            case R.id.menu_item_archive:
                ft.replace(R.id.frame_layout, MyDeliveriesFragment.newInstance(),MyDeliveriesFragment.TAG);
                ft.commit();
                return true;
            case R.id.menu_item_post:
                ft.replace(R.id.frame_layout, MyPostFragment.newInstance(),MyPostFragment.TAG);
                ft.commit();
                return true;
            case R.id.menu_item_location:
                ft.replace(R.id.frame_layout, LocationFragment.newInstance(),LocationFragment.TAG);
                ft.commit();
                return true;
        }
        return false;
    }

}
